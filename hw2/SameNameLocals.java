/* 	Nifty aspect of input-output pair:

	This program tests if the null-checker can correctly
	identify a null pointer error when a field is first correctly
	initialized but then overwritten by being assigned to another
	field that is uninitialized. 
	
	This test also involves some inheritance for some more complexity.
	A.getC() does not have a null pointer error; B.getC() does have
	a null pointer error.
*/

class SameNameLocals {
	public static void main(String[] args) {
		A a;
		B b;
		C c;
		
		a = new A();
		b = new B();
		
		c = a.getC();
		c = b.getC();
	}
}

class C {
	public boolean nullCheck() {
		return false;
	}
}

class A {
	C c1;
	C c2;
	
	public C getC() {
		boolean b;
		
		c1 = new C();
		c2 = c1;
		b = c2.nullCheck();
		
		return c2;
	}
}

class B extends A {
	public C getC() {
		boolean b;
		
		c2 = new C(); // difference is here
		c2 = c1;
		b = c2.nullCheck();
		
		return c2;
	}
}
